#include <array>
#include <chrono>
#include <iostream>
#include <ranges>
#include <thread>

int main() {
	using char_t = char;
	std::array<char_t, 80> ring_buf;
	ring_buf.fill(' ');
	auto ring_buf_start = 0z;

	auto next_time = std::chrono::steady_clock::now();
	char_t c;
	while (std::cin.get(c)) {
		if (c == '\n') {
			c = ' ';
		}
		if (c == '\t') {
			c = ' ';
		}

		std::cout.put(c);
		ring_buf[ring_buf_start] = c;
		ring_buf_start = (ring_buf_start + 1) % ring_buf.size();

		std::this_thread::sleep_until(next_time);
		std::clog.put('\r');
		auto buf_pos = ring_buf_start;
		for (auto count = 0z; count < ring_buf.size(); ++count) {
			std::clog.put(ring_buf[buf_pos]);
			buf_pos = (buf_pos + 1) % ring_buf.size();
		}
		next_time += std::chrono::milliseconds{50};
	}
}